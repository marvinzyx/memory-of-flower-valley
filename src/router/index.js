import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/home.vue')
  },
  {
    path: '/About',
    name: 'about',
    component: () => import('../views/About.vue')
  },
  {
    path: '/boke',
    name: 'boke',
    component: () => import('../views/boke.vue')
  },
  {
    path: '/classification',
    name: 'classification',
    component: () => import('../views/classification.vue')
  },
  {
    path: '/shop',
    name: 'about',
    component: () => import('../views/shop.vue'),
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('../views/home.vue'),
  },
  {
    path: '/contact',
    name: 'contact',
    component: () => import('../views/contact.vue')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/register.vue')
  },
  {
    path: '/account',
    name: 'account',
    component: () => import('../views/account.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login.vue')
  },
  {
    path: '/products',
    name: 'products',
    component: () => import('../views/products.vue')
  },
  {
    path: '/details',
    name: 'details',
    component: () => import('../views/details.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
  })
/* 
  router.beforeEach((to,from,next)=>{
    // console.log(to)
    // console.log(from)
    // next()   //至此就可以显示页面了
    //做业务逻辑,如果是登录状态，就进行下一步（注意登录页面不可做这个判断）
    if(to.name = 'login'){  
      //如果要去的页面是登录页面，就进行下一步，不做判断
      next()
    }else{
      if(localStorage.getItem('isLogin')==='ok'){
        next()
      }else{
        next('/login')
      }
    }
  }) */
export default router